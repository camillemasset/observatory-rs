use std::error::Error;
use std::process;

mod extraction;
mod models;

fn main() {
    if let Err(err) = run() {
        println!("{}", err);
        process::exit(1);
    }
}

fn run() -> Result<(), Box<dyn Error>> {
    let data = extraction::read_data()?;
    println!("{:?}", data.get(&2021).unwrap().len());
    Ok(())
}
