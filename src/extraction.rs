use std::collections::HashMap;
use std::error::Error;

use serde::Deserialize;

use super::models::*;

type LoadResult<T> = Result<T, Box<dyn Error>>;

/// Read data (stations and temperatures) from files and aggregate them by year and station.
pub fn read_data() -> LoadResult<HashMap<Year, Vec<AverageTemperature>>> {
    let year = 2021;
    let stations = read_stations("data/small_stations.csv")?;
    let year_averages = read_measurements(&format!("data/{}.csv", year))?;
    let mut all_data = HashMap::new();

    let mut temperatures = Vec::new();
    for (station_id, temperature) in year_averages.into_iter() {
        match stations.get(&station_id) {
            Some(Station { location, .. }) => {
                temperatures.push(AverageTemperature {
                    year,
                    location: *location,
                    value: temperature,
                });
            }
            None => (),
        }
    }
    all_data.insert(year, temperatures);

    Ok(all_data)
}

#[derive(Debug, Deserialize)]
struct RawStation {
    pub stn_id: Option<String>,
    pub wban_id: Option<String>,
    pub latitude: Option<f32>,
    pub longitude: Option<f32>,
}

fn read_stations(path: &str) -> LoadResult<HashMap<String, Station>> {
    let mut reader = csv::ReaderBuilder::new()
        .has_headers(false)
        .trim(csv::Trim::All)
        .from_path(path)?;

    let mut stations = HashMap::new();
    for result in reader.deserialize() {
        let record = result?;
        match parse_station_record(record) {
            Some(station) => {
                // TODO: rewrite when ownership is understood
                let station_id = station.id.clone();
                stations.insert(station_id, station);
            }
            None => (),
        }
    }

    Ok(stations)
}

fn parse_station_record(record: RawStation) -> Option<Station> {
    if record.latitude.is_some() && record.longitude.is_some() {
        Some(Station {
            id: build_station_id(record.stn_id, record.wban_id),
            location: Location {
                lat: record.latitude.unwrap(),
                lon: record.longitude.unwrap(),
            },
        })
    } else {
        None
    }
}

fn build_station_id(stn_id: Option<String>, wban_id: Option<String>) -> String {
    format!(
        "{}_{}",
        stn_id.unwrap_or("#_STN_#".to_string()),
        wban_id.unwrap_or("#_WBAN_#".to_string())
    )
}

#[derive(Debug, Deserialize)]
struct RawMeasurement {
    pub stn_id: Option<String>,
    pub wban_id: Option<String>,
    pub month: u8,
    pub day: u8,
    pub temp_f: Fahrenheit,
}

fn read_measurements(path: &str) -> LoadResult<HashMap<String, Celsius>> {
    let mut reader = csv::ReaderBuilder::new()
        .has_headers(false)
        .trim(csv::Trim::All)
        .from_path(path)?;

    // Read all measurements and group the temperatures (in °F) by station id
    let mut measurements_by_station: HashMap<String, Vec<Fahrenheit>> = HashMap::new();
    for result in reader.deserialize() {
        let record = result?;
        match parse_measurement_record(record) {
            Some((station_id, temp_f)) => measurements_by_station
                .entry(station_id)
                .or_insert(Vec::new())
                .push(temp_f),
            None => (),
        }
    }

    // Compute temperature average by station, and convert to °C
    let mut yearly_avg = HashMap::new();
    for (station_id, temperatures) in measurements_by_station {
        let avg_f: Fahrenheit = temperatures.iter().sum::<f32>() / (temperatures.len() as f32);
        let avg_c = (avg_f - 32.0) / 1.8;
        yearly_avg.insert(station_id, avg_c);
    }

    Ok(yearly_avg)
}

fn parse_measurement_record(record: RawMeasurement) -> Option<(String, Fahrenheit)> {
    if record.temp_f < 9999.9 {
        let station_id = build_station_id(record.stn_id, record.wban_id);
        Some((station_id, record.temp_f))
    } else {
        None
    }
}
