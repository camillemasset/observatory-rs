pub type Fahrenheit = f32;
pub type Celsius = f32;
pub type Year = u16;

/// Representation of a (located) weather station
#[derive(Debug)]
pub struct Station {
    pub id: String,
    pub location: Location,
}

/// Representation of a location on a sphere.
/// The latitude and longitude are in degrees.
#[derive(Clone, Copy, Debug)]
pub struct Location {
    // TODO: implement conversion functions degrees <=> radians
    /// Latitude in degrees (-90 < lat <= 90).
    pub lat: f32,
    /// Longitude in degrees (-180 < lon <= 180).
    pub lon: f32,
}

/// Simple struct holding a date (no validity check is performed).
#[derive(Copy, Clone, Debug)]
pub struct Date {
    pub year: Year,
    pub month: u8,
    pub day: u8,
}

#[derive(Debug)]
pub struct AverageTemperature {
    /// Year of the measurement.
    pub year: Year,
    /// Location of the measurement.
    pub location: Location,
    /// Temperature in degrees Celsius.
    pub value: Celsius,
}
