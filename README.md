# observatory-rs

Port of the capstone project of the Coursera specialization
[Functional Programming in Scala](https://www.coursera.org/learn/scala-capstone) in Rust.

## Setup

Install [rustup]() and run:

```sh
cargo run
```

## TODO

Unsorted list of issues to tackle.

- Add a CLI to choose the files to read.
- Add some tests
- Setup a CI
- Implement temperature interpolation
- Implement color interpolation
- Implement visualization
- ...
